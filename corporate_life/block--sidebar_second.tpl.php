<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> module"<?php print $attributes; ?>>

	<?php print render($title_prefix); ?>
		<?php if ($block->subject): ?>
		<div class="title title-on">
			<h3<?php print $title_attributes; ?>>
				<span class="first_word">
					<?php print $block->subject ?>
				</span>
			</h3>
		</div>
		<?php endif;?>
	<?php print render($title_suffix); ?>

	<div class="content modulecontent"<?php print $content_attributes; ?>>
		<?php print $content ?>
	</div>
</div>
