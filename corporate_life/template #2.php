<?php

/**
 * Add body classes if certain regions have content.
 */
function corporate_life_preprocess_html(&$variables) {
  if (!empty($variables['page']['featured'])) {
    $variables['classes_array'][] = 'featured';
}
}
?>
