<?php

/**
 * Add body classes if certain regions have content.
 */
function corporate_life_preprocess_html(&$variables) {
	if (!empty($variables['page']['featured'])) {
		$variables['classes_array'][] = 'featured';
	}
	if (!empty($variables['page']['header_bottom'])) {
		$variables['classes_array'][] = 'header_bottom';
	}

	if (!empty($variables['page']['content_top'])) {
		$variables['classes_array'][] = 'content_top';
	}
	if (!empty($variables['page']['content_bottom'])) {
		$variables['classes_array'][] = 'content_bottom';
	}
	if (!empty($variables['page']['footer_top'])) {
		$variables['classes_array'][] = 'footer_top';
	}
};

/**
 * Override or insert variables into the page template.
 */
function corporate_life_preprocess_page(&$vars) {

  // Prepare header.
  $site_fields = array();
  if (!empty($vars['site_name'])) {
    $site_fields[] = $vars['site_name'];
  }
  if (!empty($vars['site_slogan'])) {
    $site_fields[] = $vars['site_slogan'];
  }
  $vars['site_title'] = implode(' ', $site_fields);
  if (!empty($site_fields)) {
    $site_fields[0] = '<span>' . $site_fields[0] . '</span>';
  }

  // Set a variable for the site name title and logo alt attributes text.
  $slogan_text = $vars['site_slogan'];
  $site_name_text = $vars['site_name'];
  $vars['site_name_and_slogan'] = $site_name_text . ' ' . $slogan_text;
}


function corporate_life_preprocess_region(&$vars) {
  if ($vars['region'] == 'header') {
    $vars['classes_array'][] = 'clearfix';
  }
}

?>