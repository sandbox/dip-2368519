		
<div id="header_outer">
	<div id="headerwrap">
		<div id="banner">
			<div class="xtc-wrapperwide">
				<div class="slider" style="background: url(<?php print base_path() . drupal_get_path('theme', 'corporate_life'); ?>/images/90701d02ae3da0e5a21abbd900c25748_XL.jpg) top center no-repeat; width:1142px; height:404px; overflow:hidden;">
				
					<div class="greystrip">
						<p class="mainslide_title">Next Generation in Website Development</p>
						<p class="mainslide_introtext">Poster  is a new generation of automation processes content and building sites. Create your websites easily and quickly!</p>
					</div>
				
				</div>
			</div>

			<div style="clear:both"></div>
		</div>
    </div>
	
    <div id="head-bottom"></div>
    <div id="head-top"></div>
    <div id="header">
	
		<div id="logowrap" class="clearfix xtc-wrapper">
			
			<?php if ($logo || $site_title): ?>
			  <?php if ($title): ?>
				<div id="logo"><strong><a href="<?php print $front_page ?>">
				<?php if ($logo): ?>
				  <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logoimg" />
				<?php endif; ?>
				<?php print $site_html ?>
				</a></strong></div>
			  <?php else: /* Use h1 when the content title is empty */ ?>
				<h1 id="logo"><a href="<?php print $front_page ?>">
				<?php if ($logo): ?>
				  <img src="<?php print $logo ?>" alt="<?php print $site_name_and_slogan ?>" title="<?php print $site_name_and_slogan ?>" id="logoimg" />
				<?php endif; ?>
				<?php print $site_html ?>
				</a></h1>
			<?php endif; ?>
			<?php endif; ?>
			
			
		    <?php if ($secondary_menu): ?>
			  <div id="secondary-menu" class="navigation">
				<?php print theme('links__system_secondary_menu', array(
				  'links' => $secondary_menu,
				  'attributes' => array(
					'id' => 'secondary-menu-links',
					'class' => array('links', 'inline', 'clearfix'),
				  ),
				  'heading' => array(
					'text' => t('Secondary menu'),
					'level' => 'h2',
					'class' => array('element-invisible'),
				  ),
				)); ?>
			  </div> <!-- /#secondary-menu -->
			<?php endif; ?>
			
			
			<div id="language">
				<div class="english">
					<a href="http://posting.info">
						<img src="<?php print base_path() . drupal_get_path('theme', 'corporate_life'); ?>/images/united_kingdom_flag_256.png" width="25px" height="25px" />
					</a>
				</div>
				
				<div class="russian">
					<a href="http://ru.posting.info">
						<img src="<?php print base_path() . drupal_get_path('theme', 'corporate_life'); ?>/images/russia_flag_256.png" width="25px" height="25px" />
					</a>
				</div>
			</div>			
		</div>

		<div id="menu" class="xtc-wrapper">
		
		    <?php if ($main_menu): ?>
				<div id="main-menu" class="navigation">
					<?php print theme('links__system_main_menu', array(
					  'links' => $main_menu,
					  'attributes' => array(
						'id' => 'main-menu-links',
						'class' => array('menu', 'dualfish', 'clearfix'),
					  ),
					  'heading' => array(
						'text' => t('Main menu'),
						'level' => 'h2',
						'class' => array('element-invisible'),
					  ),
					)); ?>
				</div> <!-- /#main-menu -->
			<?php endif; ?>

		</div>
    </div>
        
	<div id="topmenuwrap">
		<div id="topmenu" class="xtc-wrapper">
        	<div class="moduletable iconmenu">

				<?php print render($page['header']); ?>
			
			</div>
		</div>
	</div>
</div>

<div id="region2" class="xtc-spacer xtc-wrapper">
	<div id="region2-inner" class="clearfix">

		<?php print render($page['header_bottom']); ?>
	
	</div>
</div>

<div id="region3w">
	<div class="wrap-top"></div>
	<div class="wrap-bottom"></div>
	<div id="region3wrap2" class="xtc-wrapperwide xtc-spacer">
		<div class="wrap-tl"></div>
		<div class="wrap-tr"></div>
		<div class="wrap-bl"></div>
		<div class="wrap-br"></div>
		<div id="region3" class="xtc-wrapper">
			<div class="pad">
				<div class="pad2">
					<div id="content" class="clearfix">
						<div id="center" class="xtcGrid firstcolumn" style="float:left; width:653px; margin-right:32px;">
						
							<?php print $breadcrumb; ?>
	
							<?php print render($title_prefix); ?>
							<?php if ($title): ?>
								<h1 class="title" id="page-title">
									<?php print $title; ?>
								</h1>
							<?php endif; ?>
							
							<?php if ($tabs): ?>
								<div class="tabs">
									<?php print render($tabs); ?>
								</div>
							<?php endif; ?>
							
							<?php print $messages; ?>
							<?php print render($page['help']); ?>
							
							<?php if ($action_links): ?>
								<ul class="action-links">
									<?php print render($action_links); ?>
								</ul>
							<?php endif; ?>
						  
							<div class="clearfix">
								<?php print render($page['content']); ?>
							</div>
					
						</div>

						<div id="right" class="xtcGrid lastcolumn" style="float:left; width:265px;">

							<?php if ($page['sidebar_second']): ?>
								<div id="sidebar-second" class="column sidebar">
									<div class="section">
										<?php print render($page['sidebar_second']); ?>
									</div>
								</div> <!-- /.section, /#sidebar-second -->
							<?php endif; ?>						

						</div>
					</div>
				</div>

				<div id="region3b" class="clearfix">
				
					<?php print render($page['content_bottom']); ?>
				
				</div>
			</div>
		</div>
	</div>
</div>

<div id="region4" class="xtc-wrapper clearfix xtc-spacer">

	<?php print render($page['featured']); ?>

</div>

<div id="region6">
	<div id="region6wrap2" class="xtc-wrapperwide">

		<?php print render($page['footer_top']); ?>
	
	</div>
</div>

<div id="region7">
	<div id="region7wrap2" class="xtc-wrapperwide">
		<div id="region7-inner" class="clearfix xtc-wrapper">

			<div id="footer">

				<?php print render($page['footer']); ?>
			
			</div>

		</div>
	</div>
</div>